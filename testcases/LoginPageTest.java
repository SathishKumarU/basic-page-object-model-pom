package testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.LoginPage;

public class LoginPageTest extends LoginPage{

	public LoginPageTest(WebDriver driver) {
		super(driver);
	}

	
	public static void main(String[] args) throws InterruptedException {

		LoginTest();
		ForgetTest();
		
	}
	
	public static void LoginTest () {
	System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.facebook.com/");
	
	driver.manage().window().maximize();

		
	LoginPageTest a = new LoginPageTest(driver);
	a.Email();
	a.Password();
	a.ClickLoginButton();
	
	driver.quit();
	
	}

	public static void ForgetTest() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.facebook.com/");
		
		driver.manage().window().maximize();
		LoginPageTest a = new LoginPageTest(driver);
		
		a.ForgetPasswordClick();
		a.ForgetMobileNumber();
		a.ForgetMobileNumberSearch();
		Thread.sleep(2000);
		
		a.ForgetEmail1();
		Thread.sleep(1000);
		a.ForgetPassword1();
		Thread.sleep(1000);
		a.ForgetLogin1();
	
		driver.quit();
	}
}
